using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;
class server{
    static void Main(){
        UdpClient s = new UdpClient(1234);
        IPEndPoint listener = new IPEndPoint(IPAddress.Any, 1234);
        while (true)
        {
            s.Receive(ref listener);
            s.Send(BitConverter.GetBytes((ushort)listener.Port).Concat(Encoding.ASCII.GetBytes(listener.Address.ToString())).ToArray(), 2 + listener.Address.ToString().Length, listener);
        }
    }
}